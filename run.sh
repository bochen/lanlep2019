#!/bin/bash

#change this if you think TOTAL_CORES is not correct
if [[ -z "${SLURM_CPUS_ON_NODE}" ]]; then
	export TOTAL_CORES=`grep -c ^processor /proc/cpuinfo`
else
	export TOTAL_CORES=${SLURM_CPUS_ON_NODE}
fi
echo "The node has $TOTAL_CORES cores"
export LGB_CORES=${TOTAL_CORES}

export LANLEP_HOME="$( cd "$(dirname "$0")" ; pwd -P )"
echo use home: $LANLEP_HOME

export PYTHONPATH=$LANLEP_HOME/src/main

source activate python27

cd $LANLEP_HOME/src/main
python step1_make_parq.py 			|| exit -1
python step2_make_train_segs.py		|| exit -1
python step3_make_vec_model.py		|| exit -1
python step4_make_feature.py		|| exit -1

source deactivate python27 

