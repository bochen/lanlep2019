import numpy as np 
from joblib import Parallel, delayed
from tqdm import tqdm
import utils 
from signal_feature import SignalFeature

logger = utils.get_logger("rand_proj")

n_thread = utils.get_num_thread()


def _create_feature_for_one(ss, extend=False):
    obj = SignalFeature(ss, 128, fft_freq_window=(0.2, 18.0))
    if not extend:
        x = obj.max_power, obj.freq_at_max_power, obj.spectral_edge_freq(), obj.spectral_entropy(), \
        obj.nonlinear_energy, obj.line_length, obj.rmsamp, obj.zero_cross(), obj.variance(0), obj.variance(1), \
        obj.skew(0), obj.skew(1), obj.kurtosis(0), obj.kurtosis(1), \
        obj.Hjorth_Mobility(), obj.Hjorth_Complexity(), obj.shannon_entropy(50)  # ,obj.svd_entropy()
    else:
        zc = np.fft.fft(ss)
    
        # FFT transform values
        realFFT = np.real(zc)
        imagFFT = np.imag(zc)
    
        x = obj.max_power, obj.freq_at_max_power, obj.spectral_edge_freq(), obj.spectral_entropy(), \
        obj.nonlinear_energy, obj.line_length, obj.rmsamp, obj.zero_cross(), obj.variance(0), obj.variance(1), \
        obj.skew(0), obj.skew(1), obj.kurtosis(0), obj.kurtosis(1), \
        obj.Hjorth_Mobility(), obj.Hjorth_Complexity(), obj.shannon_entropy(50), \
        realFFT.mean(), realFFT.std(), realFFT.max(), realFFT.min(), imagFFT.mean(), imagFFT.std(), imagFFT.max(), imagFFT.min()
        
    return x


def create_feature(s, offset, sublength, parallel=True, extend=False):
    N = sublength
    assert offset >= 0
    assert len(s) > N + offset
    s = s[offset:].astype(np.float)
    K = int(N / 4)
    M = int(len(s) / K)
    slist = []
    for i in range(M):
        ss = s[i * K:i * K + N]
        if len(ss) <> N : continue
        slist.append(ss)
    if parallel:
        ret = Parallel(n_jobs=n_thread)(delayed(_create_feature_for_one)(ss, extend) for ss in tqdm(slist))
    else:
        ret = [_create_feature_for_one(ss, extend) for ss in (slist)]
    return np.array(ret, dtype=np.float32)

    
def coords_to_bins(Wheels, C):
    num_wheels = Wheels[-1]['w'] + 1
    num_spokes = Wheels[-1]['s'] + 1
    pow2 = np.array([2 ** j for j in range(num_spokes)])
    Wc = np.array([w['c'] for w in Wheels])
    L = np.dot(C, np.transpose([w['p'] for w in Wheels]).conjugate())
    B = [np.dot((L[:, ws:ws + num_spokes] > Wc[ws:ws + num_spokes]), pow2) for ws in range(0, num_wheels * num_spokes, num_spokes)]
    return B


def hash_a_seg_bk(Wheels, seg, offset, sublength, parallel=False, extend=False):  # seg: raw sig
    seq = create_feature(seg, offset, sublength, parallel=parallel,extend=extend)
    return hash_a_seq(Wheels, seq,)

        
def hash_a_seq(Wheels, feat_seq):  # feat_seq 
    global NORMALIZER
    C = np.array(feat_seq, dtype=np.float)
    C = (C - NORMALIZER[0]) / NORMALIZER[1]
    if len(C) > 0:
        return coords_to_bins(Wheels, C)
    else:
        return None        


NORMALIZER = None


def set_normalizer(normalizer):
    global NORMALIZER
    m, s = normalizer
    if len(m.shape) == 1:
        m = m.reshape([1, -1])
    if len(s.shape) == 1:
        s = s.reshape([1, -1])        
    logger.info("normalizer shape {}, {} ".format(str(m.shape), str(s.shape)))
    NORMALIZER = (m, s);

        
class RandProj(object):

    def __init__(self, feat_size, hash_size):
        global NORMALIZER
        assert NORMALIZER is not None, "set_normalizer first"
        self.norm_mean = NORMALIZER[0]
        self.norm_std = NORMALIZER[1]
        self.feat_size = feat_size 
        self.hash_size = hash_size 
    
    def create_hash(self, lines):  # lines: a few feats for 1000 waves
        lines = (lines - self.norm_mean) / self.norm_std
        feats = self._sample_feats(lines)
        self._set_wheels(feats, wheels=1)
        
    def _set_wheels(self, feats, wheels=200):
        Wheels = []
        for w in range(wheels):
            logger.info("set wheels %d", w)
            Wheels += self._one_wheel(w, feats)
        Wheels.sort()
        self.Wheels = [{'w': x[0], 's': x[1], 'p': x[2], 'c': x[3]} for x in Wheels]
    
    def _one_wheel(self, w, feats):
        S = []
        for s in range(self.hash_size):
            L = [feats[u] for u in np.random.choice(range(len(feats)), size=self.feat_size, replace=False)]
            P = self._affine_hull(L, show=(s == 0))
            C = P.pop()
            S.append((w, s, P, C))
        return S

    def _affine_hull(self, linear_system, show=False):
        # linear_system: d dimensions of n docs in this hyperplane
        linear_system = np.array(linear_system)
        linear_system = np.concatenate([linear_system, np.zeros([linear_system.shape[0], 1]) - 1], axis=1)
        linear_system = np.concatenate([linear_system, np.zeros([1, linear_system.shape[1]])], axis=0)
        U, W, V = np.linalg.svd(linear_system)
        if show:
            logger.info("ls:{}, U:{}, W:{}, V:{}".format(linear_system.shape, U.shape, W.shape, V.shape))
        return list(V[-1, :])
            
    def _sample_feats(self, lines):
        total_rand_feats = self.feat_size * self.hash_size * 2
        n_reads = len(lines)
        feats = [] 
        while len(feats) < total_rand_feats:
            read = lines[int(np.random.random() * n_reads)]
            feats.append(read)
        return feats
        
    def hash_seq(self, seq):
        return hash_a_seq(self.Wheels, seq)[0]
    
