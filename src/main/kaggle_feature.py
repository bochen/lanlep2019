import numpy as np
import sys
import utils
from sklearn.linear_model import LinearRegression
from scipy.signal import hilbert
from scipy.signal import hann
from scipy.signal import convolve
from scipy import stats         
import pandas as pd
import warnings
warnings.filterwarnings("ignore")

def add_trend_feature(arr, abs_values=False):
    idx = np.array(range(len(arr)))
    if abs_values:
        arr = np.abs(arr)
    lr = LinearRegression()
    lr.fit(idx.reshape(-1, 1), arr)
    return lr.coef_[0]


def classic_sta_lta(x, length_sta, length_lta):
    
    sta = np.cumsum(x ** 2)

    # Convert to float
    sta = np.require(sta, dtype=np.float)

    # Copy for LTA
    lta = sta.copy()

    # Compute the STA and the LTA
    sta[length_sta:] = sta[length_sta:] - sta[:-length_sta]
    sta /= length_sta
    lta[length_lta:] = lta[length_lta:] - lta[:-length_lta]
    lta /= length_lta

    # Pad zeros
    sta[:length_lta - 1] = 0

    # Avoid division by zero by setting zero values to tiny float
    dtiny = np.finfo(0.0).tiny
    idx = lta < dtiny
    lta[idx] = dtiny

    return sta / lta


def np_quantile(a, q):
    return np.percentile(a, q * 100)


def np_mad(data, axis=None):
    return np.mean(np.absolute(data - np.mean(data, axis)), axis)


COLUMNS = ['mean', 'std', 'max', 'min', 'mean_change_abs', 'mean_change_rate', 'abs_max', 'abs_min', 'std_first_50000', 'std_last_50000', 'std_first_10000', 'std_last_10000', 'avg_first_50000', 'avg_last_50000', 'avg_first_10000', 'avg_last_10000', 'min_first_50000', 'min_last_50000', 'min_first_10000', 'min_last_10000', 'max_first_50000', 'max_last_50000', 'max_first_10000', 'max_last_10000', 'max_to_min', 'max_to_min_diff', 'count_big', 'mean_change_rate_first_50000', 'mean_change_rate_last_50000', 'mean_change_rate_first_10000', 'mean_change_rate_last_10000', 'q95', 'q99', 'q05', 'q01', 'abs_q95', 'abs_q99', 'abs_q05', 'abs_q01', 'trend', 'abs_trend', 'abs_mean', 'abs_std', 'mad', 'kurt', 'skew', 'med', 'Hilbert_mean', 'Hann_window_mean', 'classic_sta_lta1_mean', 'classic_sta_lta2_mean', 'classic_sta_lta3_mean', 'classic_sta_lta4_mean', 'classic_sta_lta5_mean', 'classic_sta_lta6_mean', 'classic_sta_lta7_mean', 'classic_sta_lta8_mean', 'exp_Moving_average_300_mean', 'exp_Moving_average_3000_mean', 'exp_Moving_average_30000_mean', 'MA_700MA_std_mean', 'MA_700MA_BB_high_mean', 'MA_700MA_BB_low_mean', 'MA_400MA_std_mean', 'MA_400MA_BB_high_mean', 'MA_400MA_BB_low_mean', 'MA_1000MA_std_mean', 'iqr', 'q999', 'q001', 'ave10', 'ave_roll_std_10', 'std_roll_std_10', 'max_roll_std_10', 'min_roll_std_10', 'q01_roll_std_10', 'q05_roll_std_10', 'q95_roll_std_10', 'q99_roll_std_10', 'av_change_abs_roll_std_10', 'av_change_rate_roll_std_10', 'abs_max_roll_std_10', 'ave_roll_mean_10', 'std_roll_mean_10', 'max_roll_mean_10', 'min_roll_mean_10', 'q01_roll_mean_10', 'q05_roll_mean_10', 'q95_roll_mean_10', 'q99_roll_mean_10', 'av_change_abs_roll_mean_10', 'av_change_rate_roll_mean_10', 'abs_max_roll_mean_10', 'ave_roll_std_100', 'std_roll_std_100', 'max_roll_std_100', 'min_roll_std_100', 'q01_roll_std_100', 'q05_roll_std_100', 'q95_roll_std_100', 'q99_roll_std_100', 'av_change_abs_roll_std_100', 'av_change_rate_roll_std_100', 'abs_max_roll_std_100', 'ave_roll_mean_100', 'std_roll_mean_100', 'max_roll_mean_100', 'min_roll_mean_100', 'q01_roll_mean_100', 'q05_roll_mean_100', 'q95_roll_mean_100', 'q99_roll_mean_100', 'av_change_abs_roll_mean_100', 'av_change_rate_roll_mean_100', 'abs_max_roll_mean_100', 'ave_roll_std_1000', 'std_roll_std_1000', 'max_roll_std_1000', 'min_roll_std_1000', 'q01_roll_std_1000', 'q05_roll_std_1000', 'q95_roll_std_1000', 'q99_roll_std_1000', 'av_change_abs_roll_std_1000', 'av_change_rate_roll_std_1000', 'abs_max_roll_std_1000', 'ave_roll_mean_1000', 'std_roll_mean_1000', 'max_roll_mean_1000', 'min_roll_mean_1000', 'q01_roll_mean_1000', 'q05_roll_mean_1000', 'q95_roll_mean_1000', 'q99_roll_mean_1000', 'av_change_abs_roll_mean_1000', 'av_change_rate_roll_mean_1000', 'abs_max_roll_mean_1000']


def make_feature(x):
    x = x.astype(np.float)
    feats = []
    # names = []

    def f(u, v):
        # names.append(u) 
        feats.append(v)         
        
    f('mean', x.mean())
    f('std', x.std())
    f('max', x.max())
    f('min', x.min())
    
    f('mean_change_abs', np.mean(np.diff(x)))
    f('mean_change_rate', np.mean(np.nonzero((np.diff(x) / x[:-1]))[0]))
    f('abs_max', np.abs(x).max())
    f('abs_min', np.abs(x).min())
    
    f('std_first_50000', x[:50000].std())
    f('std_last_50000', x[-50000:].std())
    f('std_first_10000', x[:10000].std())
    f('std_last_10000', x[-10000:].std())
    
    f('avg_first_50000', x[:50000].mean())
    f('avg_last_50000', x[-50000:].mean())
    f('avg_first_10000', x[:10000].mean())
    f('avg_last_10000', x[-10000:].mean())
    
    f('min_first_50000', x[:50000].min())
    f('min_last_50000', x[-50000:].min())
    f('min_first_10000', x[:10000].min())
    f('min_last_10000', x[-10000:].min())
    
    f('max_first_50000', x[:50000].max())
    f('max_last_50000', x[-50000:].max())
    f('max_first_10000', x[:10000].max())
    f('max_last_10000', x[-10000:].max())
    
    f('max_to_min', x.max() / np.abs(x.min()))
    f('max_to_min_diff', x.max() - np.abs(x.min()))
    f('count_big', len(x[np.abs(x) > 500]))
    # f('sum', x.sum())
    
    f('mean_change_rate_first_50000', np.mean(np.nonzero((np.diff(x[:50000]) / x[:50000][:-1]))[0]))
    f('mean_change_rate_last_50000', np.mean(np.nonzero((np.diff(x[-50000:]) / x[-50000:][:-1]))[0]))
    f('mean_change_rate_first_10000', np.mean(np.nonzero((np.diff(x[:10000]) / x[:10000][:-1]))[0]))
    f('mean_change_rate_last_10000', np.mean(np.nonzero((np.diff(x[-10000:]) / x[-10000:][:-1]))[0]))
    
    f('q95', np_quantile(x, 0.95))
    f('q99', np_quantile(x, 0.99))
    f('q05', np_quantile(x, 0.05))
    f('q01', np_quantile(x, 0.01))
    
    f('abs_q95', np_quantile(np.abs(x), 0.95))
    f('abs_q99', np_quantile(np.abs(x), 0.99))
    f('abs_q05', np_quantile(np.abs(x), 0.05))
    f('abs_q01', np_quantile(np.abs(x), 0.01))
    
    f('trend', add_trend_feature(x))
    f('abs_trend', add_trend_feature(x, abs_values=True))
    f('abs_mean', np.abs(x).mean())
    f('abs_std', np.abs(x).std())
    
    f('mad', np_mad(x))
    f('kurt', stats.kurtosis(x))
    f('skew', stats.skew(x))
    f('med', np.median(x))
    
    f('Hilbert_mean', np.abs(hilbert(x)).mean())
    f('Hann_window_mean', (convolve(x, hann(150), mode='same') / sum(hann(150))).mean())
    f('classic_sta_lta1_mean', classic_sta_lta(x, 500, 10000).mean())
    f('classic_sta_lta2_mean', classic_sta_lta(x, 5000, 100000).mean())
    f('classic_sta_lta3_mean', classic_sta_lta(x, 3333, 6666).mean())
    f('classic_sta_lta4_mean', classic_sta_lta(x, 10000, 25000).mean())
    f('classic_sta_lta5_mean', classic_sta_lta(x, 50, 1000).mean())
    f('classic_sta_lta6_mean', classic_sta_lta(x, 100, 5000).mean())
    f('classic_sta_lta7_mean', classic_sta_lta(x, 333, 666).mean())
    f('classic_sta_lta8_mean', classic_sta_lta(x, 4000, 10000).mean())
    sx = pd.Series(x)
    Moving_average_700_mean = sx.rolling(window=700).mean().mean(skipna=True)
    # f( 'Moving_average_700_mean',Moving_average_700_mean )
    ewma = pd.Series.ewm
    f('exp_Moving_average_300_mean', (ewma(sx, span=300).mean()).mean(skipna=True))
    f('exp_Moving_average_3000_mean', ewma(sx, span=3000).mean().mean(skipna=True))
    f('exp_Moving_average_30000_mean', ewma(sx, span=6000).mean().mean(skipna=True))
    no_of_std = 3
    MA_700MA_std_mean = sx.rolling(window=700).std().mean()
    f('MA_700MA_std_mean', MA_700MA_std_mean)
    f('MA_700MA_BB_high_mean', (Moving_average_700_mean + no_of_std * MA_700MA_std_mean).mean())
    f('MA_700MA_BB_low_mean', (Moving_average_700_mean - no_of_std * MA_700MA_std_mean).mean())
    MA_400MA_std_mean = sx.rolling(window=400).std().mean()
    f('MA_400MA_std_mean', MA_400MA_std_mean)
    f('MA_400MA_BB_high_mean', (Moving_average_700_mean + no_of_std * MA_400MA_std_mean).mean())
    f('MA_400MA_BB_low_mean', (Moving_average_700_mean - no_of_std * MA_400MA_std_mean).mean())
    f('MA_1000MA_std_mean', sx.rolling(window=1000).std().mean())
    
    f('iqr', np.subtract(*np.percentile(x, [75, 25])))
    f('q999', np_quantile(x, 0.999))
    f('q001', np_quantile(x, 0.001))
    f('ave10', stats.trim_mean(x, 0.1))

    for windows in [10, 100, 1000]:
        x_roll_std = sx.rolling(windows).std().dropna().values
        x_roll_mean = sx.rolling(windows).mean().dropna().values
        
        f('ave_roll_std_' + str(windows), x_roll_std.mean())
        f('std_roll_std_' + str(windows), x_roll_std.std())
        f('max_roll_std_' + str(windows), x_roll_std.max())
        f('min_roll_std_' + str(windows), x_roll_std.min())
        f('q01_roll_std_' + str(windows), np_quantile(x_roll_std, 0.01))
        f('q05_roll_std_' + str(windows), np_quantile(x_roll_std, 0.05))
        f('q95_roll_std_' + str(windows), np_quantile(x_roll_std, 0.95))
        f('q99_roll_std_' + str(windows), np_quantile(x_roll_std, 0.99))
        f('av_change_abs_roll_std_' + str(windows), np.mean(np.diff(x_roll_std)))
        f('av_change_rate_roll_std_' + str(windows), np.mean(np.nonzero((np.diff(x_roll_std) / x_roll_std[:-1]))[0]))
        f('abs_max_roll_std_' + str(windows), np.abs(x_roll_std).max())
        
        f('ave_roll_mean_' + str(windows), x_roll_mean.mean())
        f('std_roll_mean_' + str(windows), x_roll_mean.std())
        f('max_roll_mean_' + str(windows), x_roll_mean.max())
        f('min_roll_mean_' + str(windows), x_roll_mean.min())
        f('q01_roll_mean_' + str(windows), np_quantile(x_roll_mean, 0.01))
        f('q05_roll_mean_' + str(windows), np_quantile(x_roll_mean, 0.05))
        f('q95_roll_mean_' + str(windows), np_quantile(x_roll_mean, 0.95))
        f('q99_roll_mean_' + str(windows), np_quantile(x_roll_mean, 0.99))
        f('av_change_abs_roll_mean_' + str(windows), np.mean(np.diff(x_roll_mean)))
        f('av_change_rate_roll_mean_' + str(windows), np.mean(np.nonzero((np.diff(x_roll_mean) / x_roll_mean[:-1]))[0]))
        f('abs_max_roll_mean_' + str(windows), np.abs(x_roll_mean).max())

    return np.array(feats, dtype=np.float32)


if __name__ == '__main__':
        s = np.random.random(size=[150000])
        
        feat = make_feature(s)
        print len(feat), len(COLUMNS)
        print  feat
