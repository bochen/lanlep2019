import fastparquet 
import pandas as pd 
import numpy as np
import config, utils
from task import Task
import os
from data import QuakeSeg
from joblib import Parallel, delayed
 
    
def train_quake_seg_seq(offset=0):

    def f(logger):
        for quakeno in range(17):
            N = 150 * 1000
            fpath = os.path.join(config.INPUT_PATH, "quake_{}.parq".format(quakeno))
            assert utils.file_exists(fpath), fpath
            
            df = fastparquet.ParquetFile(fpath).to_pandas()
            df = df.iloc[offset:]
            assert len(df) > N
            
            for i in range(len(df) / N):
                subdf = df.iloc[i * N : (i + 1) * N]
                fname = config.get_seg_path("quake_{:02d}_{:04d}.npz".format(quakeno, i), "train_seq_offset_{:06d}".format(offset))
                logger.info("write " + fname)            
                qs = QuakeSeg(df=subdf, name="quake_{:02d}_{:04d}".format(quakeno, i))
                qs.save(fname)

    task = Task('train_quake_seg_seq-offset_{}'.format(offset), lambda u: f(u))
    task()


def dense_train_quake_seg_seq(offsets):

    def f(logger, offset):
        for quakeno in range(17):
            N = 150 * 1000
            fpath = os.path.join(config.INPUT_PATH, "quake_{}.parq".format(quakeno))
            assert utils.file_exists(fpath), fpath
            
            df = fastparquet.ParquetFile(fpath).to_pandas()
            df = df.iloc[offset:]
            assert len(df) > N
            
            for i in range(len(df) / N):
                subdf = df.iloc[i * N : (i + 1) * N]
                fname = config.get_seg_path("quake_{:02d}_{:04d}_{:06d}.npz".format(quakeno, i, offset), "dense_train_seq".format(offset))
                logger.info("write " + fname)            
                qs = QuakeSeg(df=subdf, name="quake_{:02d}_{:04d}_{:06d}".format(quakeno, i, offset))
                qs.save(fname)

    for offset in offsets:
        task = Task('dense_train_quake_seg_seq-offset_{}'.format(offset), lambda u: f(u, offset))
        task()


def train_quake_rand_seg_seq(coverage):

    def process_one(quakeno):
            N = 150 * 1000
            fpath = os.path.join(config.INPUT_PATH, "quake_{}.parq".format(quakeno))
            assert utils.file_exists(fpath), fpath
            
            df = fastparquet.ParquetFile(fpath).to_pandas()
            assert len(df) > N            
            M = int(len(df) / float(N) * coverage)
            for _ in range(M):
                offset = int(np.random.random() * (len(df) - N))
                subdf = df.iloc[offset : offset + N]
                fname = config.get_seg_path("quake_{:02d}_o{}.npz".format(quakeno, offset), "train_seq_coverage_{}".format(coverage))
                print("write " + fname)            
                qs = QuakeSeg(df=subdf, name="quake_{:02d}_o{}".format(quakeno, offset))
                qs.save(fname)
        
    def f(logger):
        Parallel(n_jobs=utils.get_num_thread())(delayed(process_one)(quakeno) for quakeno in range(17))
            
    task = Task('train_quake_rand_seg_seq-coverage_{}'.format(coverage), lambda u: f(u))
    task()


def special_train_quake_rand_seg_seq(coverage):

    def process_one(quakeno):
            N = 150 * 1000
            fpath = os.path.join(config.INPUT_PATH, "quake_{}.parq".format(quakeno))
            assert utils.file_exists(fpath), fpath
            
            df = fastparquet.ParquetFile(fpath).to_pandas()
            assert len(df) > N            
            df1 = df[df['time_to_failure'] <= 4.5]
            df2 = df[ (df['time_to_failure'] > 4.5) & (df['time_to_failure'] < 9.5)]
            assert len(df1) > 10 ** 6
            assert len(df2) > 10 ** 6
            M = int(len(df2) / float(N) * coverage / 2)
            print("quake {}, len={},  two-part-split {}/{}".format(quakeno, len(df), M * 2, M))
            df = df1
            for _ in range(2 * M):
                offset = int(np.random.random() * (len(df) - N))
                subdf = df.iloc[offset : offset + N]
                fname = config.get_seg_path("quake_{:02d}_o{}.npz".format(quakeno, offset), "sp_train_seq_coverage_{}".format(coverage))
                print("write " + fname)            
                qs = QuakeSeg(df=subdf, name="quake_{:02d}_o{}".format(quakeno, offset))
                qs.save(fname)
                
            df = df2
            offset1 = len(df1)
            for _ in range(M):
                offset = int(np.random.random() * (len(df) - N))
                subdf = df.iloc[offset : offset + N]
                fname = config.get_seg_path("quake_{:02d}_o{}.npz".format(quakeno, offset + offset1), "sp_train_seq_coverage_{}".format(coverage))
                print("write " + fname)            
                qs = QuakeSeg(df=subdf, name="quake_{:02d}_o{}".format(quakeno, offset + offset1))
                qs.save(fname)                
        
    def f(logger):
        Parallel(n_jobs=utils.get_num_thread())(delayed(process_one)(quakeno) for quakeno in range(1, 16))
            
    task = Task('special_train_quake_rand_seg_seq-coverage_{}'.format(coverage), lambda u: f(u))
    task()

               
def run():
        for offset in [0, 37500, 75000, 112500]:
            train_quake_seg_seq(offset=offset)
            
        offsets = range(0, 150000, 1500)
        dense_train_quake_seg_seq(offsets)
        
        # for quakes  in [[ 9,  4,  6] , [15,  1,  5], [10, 14,  3],  [7, 11,  2],  [8, 13, 12]]:
        for cv in [18, 19, 20, 21, 22]:
            train_quake_rand_seg_seq(coverage=cv)
        for cv in [18, 19, 20, 21, 22]:
            special_train_quake_rand_seg_seq(coverage=cv)


if __name__ == '__main__':
        run()

