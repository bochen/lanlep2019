'''
Created on Apr 29, 2018

@author: bo
'''

import dask.dataframe as dd 
import sys 
import numpy as np 
if __name__ == '__main__':
    fname = sys.argv[1]
    print "Check", fname 
    df = dd.read_parquet(fname) 
    for col in df.columns:
        print "\t", col
    
