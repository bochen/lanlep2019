import numpy as np
import scipy.stats
import sys
import utils
         
def freq_fft(signal,sample_rate=None,window=(-np.inf,np.inf)):
    import scipy.fftpack
    a=scipy.fftpack.fft(signal)
    if sample_rate is None:
        sample_rate = np.max([1, int(len(signal)/128.)])
    if 1:
        f=(1.0*sample_rate/np.array(range(1,len(signal)+1)))[::-1]
        l,h=window
        idx=np.where((f>=l) & (f<h))
        return f[idx],np.abs(a[idx])**2

def ar_error(s1,s2,order=32):
    from nitime import algorithms as alg
    def ar_pred(s,coefs):
        m=len(coefs)
        xhat=[]
        for i in range(len(s)-m):
            xhat.append(np.sum(s[i:i+m]*coefs))
        a=s[m:]
        b=np.array(xhat)
        return a,b,np.sqrt(np.mean((a-b)**2))
    if s1 is None: return 0.0
    try:
        coefs_est, _ = alg.AR_est_YW(s1, order)
        _,_,err1=ar_pred(s1,coefs_est)
        _,_,err2=ar_pred(s2,coefs_est)
        return (err2+1e-10)/(err1+1e-10)        
    except:
        return 0
    
class SignalFeature(object):
    def __init__(self, signal,fft_sample_rate=400, fft_freq_window=(0.2,18.0)):
        self.signal= signal
        self.fft_freq_window=fft_freq_window 
        self.fft_sample_rate=fft_sample_rate
        self.fft = freq_fft(signal,sample_rate=fft_sample_rate,window=fft_freq_window)
    
    @property
    def freq(self):
        freq,power=self.fft
        return     freq 
    @property
    def power2(self):
        freq,power=self.fft
        return     power
    
    
    def power_band(self,min_f=-np.inf, max_f=np.inf):
        freq,power=self.fft
        return np.sqrt(np.sum(power[(freq<max_f) & (freq>=min_f)]))
    
    
    @property
    def max_power(self):
        return np.sqrt(np.max(self.power2))

    @property
    def freq_at_max_power(self):
        return self.freq[np.argmax(self.power2)]

    def spectral_edge_freq(self, percent=0.95):
        cusum_power=(np.cumsum(self.power2)/np.sum(self.power2))
        return self.freq[np.sum(cusum_power<=percent)-1]
    
    def spectral_entropy(self, num_sample_in_bin=50):
        power=self.power2
        bins=int(len(power)/num_sample_in_bin)
        p=np.histogram(power,bins=bins)[0]/float(len(power))+1e-7
        return -(np.sum(p*np.log(p)))

    ####################### time domain features #####################
    
    @property
    def nonlinear_energy(self):
        signal=self.signal
        return np.mean(signal[1:-1]**2-signal[:-2]*signal[2:])        
    @property
    def line_length(self):
        signal=self.signal            
        return (np.sum(np.abs(signal[1:]-signal[:-1])))
    @property
    def rmsamp(self):
        signal=self.signal
        return         (np.sqrt(np.mean(np.sum(signal**2))))

    def zero_cross(self,order=0):
        def f(x):
            x=x-np.mean(x)
            return np.sum([(x[i]<0.01 and x[i+5]>0.01) for i in range(len(x)-6)])
        return         (f(np.diff(self.signal,order)))

    
    def variance(self,order):
        return         (np.var(np.diff(self.signal,order)))
    
    def skew(self,order):
        return         (scipy.stats.skew(np.diff(self.signal,order)))
    
    def kurtosis(self,order):
        return         (scipy.stats.kurtosis(np.diff(self.signal,order)))
    
    def Hjorth_Mobility_Complexity(self):
        def f():
            f_Hjorth_Mobility= lambda signal: np.sqrt(np.var(np.diff(signal))/np.var(signal))
            hm=f_Hjorth_Mobility(self.signal)
            hj=f_Hjorth_Mobility(np.diff(self.signal))/hm
            return hm,hj
        prop_name = "_" + sys._getframe().f_code.co_name
        return utils.set_if_not_exists(self, prop_name, f)
        
        return 
    def Hjorth_Mobility(self):
        return self.Hjorth_Mobility_Complexity()[0]
    def Hjorth_Complexity(self):
        return self.Hjorth_Mobility_Complexity()[1]

    def shannon_entropy(self, num_sample_in_bin):
        signal=self.signal
        bins=int(len(signal)/num_sample_in_bin)
        p=np.histogram(signal,bins=bins,normed=False)[0]/float(len(signal))+1e-8
        return -(np.sum(p*np.log(p)))    
    
    def svd_entropy(self, dg=20,tao=2):
        signal=self.signal        
        Y=[]
        i=0
        while True:
            if i+(dg-1)*tao<len(signal):
                Y.append(signal[i:i+(dg-1)*tao])
            else:
                break
            i+=1
        Y=np.array(Y)
        sigma=np.linalg.svd(Y)[1][:10]
        p=sigma/np.sum(sigma)+1e-10
        return -(np.sum(p*np.log(p))) 

COLUMNS=['max_power', 'freq_at_max_power', 'spectral_edge_freq', 'spectral_entropy', 'nonlinear_energy', 'line_length', 'rmsamp', 'zero_cross0', 'zero_cross1', 'variance1', 'skew1', 'kurtosis1', 'Hjorth_Mobility', 'Hjorth_Complexity', 'shannon_entropy']            

def make_feature(x):
    ss = x.astype(np.float)
    feats = []
    #names = []

    def f(u, v):
        #names.append(u) 
        feats.append(v)         
    
    obj = SignalFeature(ss, 128, fft_freq_window=(0.2, 18.0))
    f('max_power', obj.max_power)
    f('freq_at_max_power', obj.freq_at_max_power)
    f('spectral_edge_freq', obj.spectral_edge_freq())
    
    f('spectral_entropy', obj.spectral_entropy())
    f('nonlinear_energy', obj.nonlinear_energy)
    f('line_length', obj.line_length)
    f('rmsamp', obj.rmsamp)
    f('zero_cross0', obj.zero_cross(0))
    f('zero_cross1', obj.zero_cross(1))
    f('variance1', obj.variance(1))
    f('skew1', obj.skew(1))
    f('kurtosis1', obj.kurtosis(1))
    f('Hjorth_Mobility', obj.Hjorth_Mobility())
    f('Hjorth_Complexity', obj.Hjorth_Complexity())
    f('shannon_entropy', obj.shannon_entropy(50))
    
    return np.array(feats, dtype=np.float32)


if __name__ == '__main__':
        s = np.random.random(size=[150000])
        
        feat = make_feature(s)
        print len(feat), len(COLUMNS)
        print  feat
