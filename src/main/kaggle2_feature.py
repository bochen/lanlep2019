import numpy as np
import sys
import utils
from sklearn.linear_model import LinearRegression
from scipy.signal import hilbert
from scipy.signal import hann
from scipy.signal import convolve
from scipy import stats         
import pandas as pd
import warnings
warnings.filterwarnings("ignore")

COLUMNS = ['Rmean', 'Rstd', 'Rmax', 'Rmin', 'Imean', 'Istd', 'Imax', 'Imin', 'Rmean_last_5000', 'Rstd__last_5000', 'Rmax_last_5000', 'Rmin_last_5000', 'Rmean_last_15000', 'Rstd_last_15000', 'Rmax_last_15000', 'Rmin_last_15000']

def make_feature(x):
    x = x.astype(np.float)
    feats = []
    #names = []

    zc = np.fft.fft(x)
    
    #FFT transform values
    realFFT = np.real(zc)
    imagFFT = np.imag(zc)
    
    def f(u, v):
        #names.append(u) 
        feats.append(v)         

    f('Rmean', realFFT.mean())
    f('Rstd',realFFT.std())
    f('Rmax',realFFT.max())
    f('Rmin',realFFT.min())
    f('Imean',imagFFT.mean())
    f('Istd',imagFFT.std())
    f('Imax',imagFFT.max())
    f('Imin',imagFFT.min())
    f('Rmean_last_5000',realFFT[-5000:].mean())
    f('Rstd__last_5000',realFFT[-5000:].std())
    f('Rmax_last_5000',realFFT[-5000:].max())
    f('Rmin_last_5000',realFFT[-5000:].min())
    f('Rmean_last_15000',realFFT[-15000:].mean())
    f('Rstd_last_15000',realFFT[-15000:].std())
    f('Rmax_last_15000',realFFT[-15000:].max())
    f('Rmin_last_15000',realFFT[-15000:].min())
    #print names
    return np.array(feats, dtype=np.float32)


if __name__ == '__main__':
        s = np.random.random(size=[150000])
        
        feat = make_feature(s)
        print len(feat), len(COLUMNS)
        print  feat
