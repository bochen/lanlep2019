import fastparquet 
import pandas as pd 
import numpy as np
import config, utils
from task import Task
import os
from joblib import Parallel, delayed
from tqdm import tqdm 
import vec_feature2
from multiprocessing import Pool
import sys

logger = utils.get_logger("process_one_for_make_vecext_feature")
    
def process_one_for_make_vec_feature(abc):
    segname, segpath, offset = abc
    logger.info("processing " + segpath)
    global g_vec_featmaker
    x, y = g_vec_featmaker.make_seg_feature(segpath, offset=offset, sublength=1200)
    return segname, x, y 


def make_vec_feature_ext(name, modelpath, offset):
        
    def f(logger):
        d = config.get_segs(name)

        normalizerpath = os.path.join(config.VEC_PATH, "normalizer.pklext")
        hasherpath = os.path.join(config.VEC_PATH, "hasher.pklext")
        global g_vec_featmaker
        g_vec_featmaker = vec_feature2.FeatureMaker(modelpath, normalizerpath, hasherpath)
        
        p = Pool(utils.get_num_thread())
        params = [ (u[0], u[1], offset) for u in d.items()]
        ret = p.map(process_one_for_make_vec_feature, params)
        p.close()
        p.join()

        indexes = [u[0] for u in ret]
        X = [u[1] for u in ret]
        Y = [u[2] for u in ret]
        logger.info("{} out of {} segs are None".format(np.sum([u is None for u in X]), len(X)))        
        X = [np.zeros([100]) if u is None else u for u in X]
        df = pd.DataFrame(X, index=indexes, dtype=np.float32, columns=['fwv_' + str(u) for u in range(100)])
        df['y'] = Y
        logger.info("df.shape={}\n{}".format(df.shape, str(df.head())))
        for col in df.columns:
            logger.info("null of {}: {}".format(col, df[col].isnull().mean()))
        
        outname = os.path.join(config.FEAT_PATH, name + "_fwvext_offset_{}.parq".format(offset))
        logger.info("write " + outname)
        fastparquet.write(outname, df)
        
    task = Task('make_vec_feature-ext_{}_offset_{}'.format(name, offset), lambda u: f(u))
    task()



def run_vec(name, offset):
    modelpath = os.path.join(config.VEC_PATH, 'modelext.vec')
    make_vec_feature_ext(name, modelpath, offset)


if __name__ == '__main__':
    if len(sys.argv) == 4 and sys.argv[1] == 'vec':
        run_vec(name=sys.argv[2], offset=int(sys.argv[3]))
    else:
        raise Exception("error")

