'''
Created on Feb 7, 2019

@author:  Lizhen Shi
'''
import numpy as np 
import config
import utils
import json
import fastparquet
import os
import pandas as pd 


class QuakeSeg(object):

    def __init__(self, signal=None, failtime=None, df=None, name=""):
        self.name = name
        if signal is not None:
            self.signal = signal.astype(np.int16)
            self.failtime = failtime
        elif df is not None:
            self.signal = df['acoustic_data'].values.astype(np.int16)
            if 'time_to_failure' in df:
                self.failtime = df['time_to_failure'].values[-1]
            else:
                self.failtime = np.nan
        else:
            raise Exception("Args are wrong")

    def save(self, path):
        np.savez_compressed(path, name=np.array([self.name]), failtime=np.array([self.failtime]), signal=self.signal)
    
    @staticmethod
    def load(path):
        d = np.load(path)
        name = d['name'][0]
        failtime = float(d['failtime'][0])
        signal = d['signal']
        
        return QuakeSeg(name=name, signal=signal, failtime=failtime)
    
    
def load_segs(category1="", category2=""):
    d = config.get_segs(category1=category1, category2=category2)
    segs = {}
    for k, v in d.items():
        segs[k] = QuakeSeg.load(v)
    return segs
         
        
def _make_feature_quake_funs(qs, funs):  # fun=f(sig)
    x = [fun(qs.signal) for fun in funs]
    return np.array(x, dtype=np.float32), qs.failtime


train_test_configs = {
    "exclude_rand_all": dict()
    }


class TrainTest(object):
    
    def __init__(self, dataname, coverages=None, offset=None, types=None, valid_strategy=None, transform=None, seed=None):  # strategy: {'type':'rand', test_ratio:0.2}, {'type':'quake', test_quakes:[2,3,4]}
        # dataname: 'coverage' or 'offset'
        # coverages = [18,19]
        # offset 37500
        # types = ["kaggle",'fwv','signal']
        self.logger=utils.get_logger('TrainTest')        
        if dataname == 'offset': assert offset is not None
        assert types is not None
        self.dataname = dataname
        self.transform = transform
        self.offset = offset
        self.types = types
        self.seed = seed
        self.valid_strategy = valid_strategy
        if coverages is None:
            coverages = [18, 19, 20, 21, 22]
        self.coverages = coverages
    
        self._load_data()
        
    def _make_splits(self, df):
        strategy = self.valid_strategy
        # {'type':'rand', test_ratio:0.2}
        if strategy['type'] == 'rand' and "test_ratio" in strategy:
            test_ratio = strategy['test_ratio']
            assert test_ratio > 0 and test_ratio < 1
            indexes = np.random.permutation(range(len(df)))
            N = int(len(df) * test_ratio)
            test_indexes = indexes[:N]
            train_indexes = indexes[N:]
            return [(train_indexes, test_indexes)]
        elif strategy['type'] == 'rand' and "fold" in strategy:
            fold = strategy['fold']
            assert fold > 0 
            indexes = np.random.permutation(range(len(df)))
            ret = []
            for i in range(fold):
                test_indexes = [u for u in indexes if u % fold == i]
                train_indexes = [u for u in indexes if u % fold <> i]
                ret.append((train_indexes, test_indexes))
            return ret
        elif strategy['type'] in[ 'byquake', 'byquake']:
            testquakes = strategy['test_quakes']
            assert isinstance(testquakes, list)
            if not isinstance(testquakes[0], list):
                testquakes = [testquakes]
                
            quake_array = df.index.map(lambda u: int(u.split("_")[1]))
            indexes = np.array(range(len(df)))
            
            ret = []
            for quakes in testquakes:
                quakes = set(quakes)
                test_indexes = indexes[[u in quakes for u in quake_array]]
                train_indexes = indexes[[u not in quakes for u in quake_array]]
                np.random.shuffle(test_indexes)
                np.random.shuffle(train_indexes)
                ret.append((train_indexes, test_indexes))
            return ret
         
    def _load_data(self):

        def _load():

            def _load_parq(fname):
                return fastparquet.ParquetFile(os.path.join(config.FEAT_PATH, fname)).to_pandas()

            def _load_type(prefix, t):
                if t == 'kaggle':
                    return _load_parq(prefix + ".parq")
                elif t == 'signal': 
                    return _load_parq(prefix + "_signal.parq")
                elif t == 'kaggle2': 
                    return _load_parq(prefix + "_kaggle2.parq")                
                elif t == 'fwvsoftmax': 
                    return _load_parq(prefix + "_fwvsoftmax_pca10_offset_.parq")                
                elif t == 'fwvdis': 
                    return _load_parq(prefix + "_fwvdis_pca10_offset_.parq")
                elif t == 'fwvdis2': 
                    return _load_parq(prefix + "_fwvdis2_pca10_offset_.parq")                
                elif t == 'fwvcos': 
                    return _load_parq(prefix + "_fwvcos_pca10_offset_.parq")                
                elif t == 'fwv': 
                    a = _load_parq(prefix + "_fwv_offset_0.parq")
                    b = _load_parq(prefix + "_fwv_offset_125.parq")
                    return (a + b) / 2
                elif t == 'fwvextsoftmax': 
                    return _load_parq(prefix + "_fwvextsoftmax_pca10_offset_.parq")                
                elif t == 'fwvextdis': 
                    return _load_parq(prefix + "_fwvextdis_pca10_offset_.parq")
                elif t == 'fwvextdis2': 
                    return _load_parq(prefix + "_fwvextdis2_pca10_offset_.parq")                
                elif t == 'fwvextcos': 
                    return _load_parq(prefix + "_fwvextcos_pca10_offset_.parq")                
                elif t == 'fwvext': 
                    a = _load_parq(prefix + "_fwvext_offset_0.parq")
                    b = _load_parq(prefix + "_fwvext_offset_150.parq")
                    return (a + b) / 2                
                elif t == 'fwvwholereg': 
                    return _load_parq(prefix + "_fwvwholereg.parq")                
                elif t == 'fwvextwholereg': 
                    return _load_parq(prefix + "_fwvextwholereg.parq")                
                else:
                    raise Exception("unknown " + t)                

            def _load_sd(t):
                return None
                if t == 'kaggle':
                    return np.load(os.path.join(config.FEAT_PATH, 'kaggle_mean_std.pkl'))
                elif t == 'signal': 
                    return np.load(os.path.join(config.FEAT_PATH, 'signal_mean_std.pkl'))
                elif t == 'fwv': 
                    return np.load(os.path.join(config.FEAT_PATH, 'fwv_mean_std.pkl'))
                else:
                    #raise Exception("unknown " + t)
                    print "warning, unknown "+t;
                    return None                

            def _merge_sds(lst):
                d = {}
                keys = ['mean', 'med', 'std', 'q1', 'q3', 'iqr']
                for u in keys:
                    has_none=False
                    for v in lst:
                        if v is None:
                            has_none=True
                            break
                    if has_none:
                        d[u] = None
                    else:
                        d[u] = pd.concat([v[u] for v in lst])
                return d
            
            if self.dataname == "offset":
                trains = []
                tests = []
                sd_means = []
                for t in self.types:
                    tests.append(_load_type('test', t))
                    trains.append(_load_type('train_seq_offset_{:06d}'.format(self.offset), t))
                    sd_means.append(_load_sd(t))
                    
                test = pd.concat(tests, axis=1).drop("y", axis=1)
                df = pd.concat(trains, axis=1)
                train = df.drop('y', axis=1)
                ydf = df[['y']]
                std_mean = _merge_sds(sd_means)
                return train, ydf, std_mean, test
            elif self.dataname in ["coverage", 'coveragesp','dense']:
                trains = []
                tests = []
                sd_means = []
                for t in self.types:
                    tests.append(_load_type('test', t))
                    sd_means.append(_load_sd(t))
                
                if (self.dataname=='dense'):
                    lst = []
                    for t in self.types:
                        lst.append(_load_type('dense_train_seq', t))
                    abc = pd.concat(lst, axis=1)
                    trains.append(abc)                        
                else:
                    for o in self.coverages:
                        lst = []
                        for t in self.types:
                            if(self.dataname=='coverage'):
                                lst.append(_load_type('train_seq_coverage_{}'.format(o), t))
                            elif(self.dataname=='coveragesp'):
                                lst.append(_load_type('sp_train_seq_coverage_{}'.format(o), t))
                    abc = pd.concat(lst, axis=1)
                    trains.append(abc)
                
                test = pd.concat(tests, axis=1).drop("y", axis=1)
                df = pd.concat(trains, axis=0)
                train = df.drop('y', axis=1)
                ydf = df[['y']]
                std_mean = _merge_sds(sd_means)
                return train, ydf, std_mean, test
                    
        train, ydf, std_mean, test = _load()
        self.testdf = test
        self.traindf = train
        assert 'y' not in self.traindf.columns
        assert  (self.traindf.columns == self.testdf.columns).all()
        self.ydf = ydf.iloc[:,0]
        if 0:
            self.logger.info("testdf.head():\n" + str(self.testdf.head()))
            self.logger.info("traindf.head():\n" + str(self.traindf.head()))
            self.logger.info("ydf.head():\n" + str(self.ydf.head()))
        if self.transform is None:
            pass
        elif self.transform == 'standard':
            s = std_mean['std']
            m = std_mean['mean']
            s=s.loc[self.testdf.columns]
            m=m.loc[self.testdf.columns]
            assert (s.index == m.index).all()
            #print zip(self.testdf.columns, s.index)
            assert (self.testdf.columns == s.index).all()
            assert (self.traindf.columns == s.index).all()
            self.testdf = (self.testdf-m)/s
            self.traindf = (self.traindf.reset_index(drop=True)-m)/s
            if 0:
                self.logger.info("testdf.head():\n" + str(self.testdf.head()))
                self.logger.info("traindf.head():\n" + str(self.traindf.head()))
            
        elif self.transform == 'standard_median':
            s = std_mean['iqr']
            m = std_mean['med']
            assert (s.index == m.index).all()
            assert (self.testdf.columns == s.index).all()
            assert (self.traindf.columns == s.index).all()
            self.testdf = (self.testdf-m)/s
            self.traindf = (self.traindf.reset_index(drop=True)-m)/s
            if 0:
                self.logger.info("testdf.head():\n" + str(self.testdf.head()))
                self.logger.info("traindf.head():\n" + str(self.traindf.head()))
        else:
            raise Exception("unknown " + self.transform)
        if self.valid_strategy is None: 
            pass
        else:
            self.splits = self._make_splits(self.traindf)
            
    def num_dataset(self):
        if self.valid_strategy is None:
            return 1
        else:
            return len(self.splits)
        
    def get_test_data(self):
        return self.testdf
    
    def get_train_data(self, i):
        assert i < self.num_dataset()
        if self.valid_strategy is None:
            return self.traindf, self.ydf, None,None
        else:
            train_indexes, test_indexes = self.splits[i]
            return self.traindf.iloc[train_indexes], self.ydf.iloc[train_indexes], \
                self.traindf.iloc[test_indexes], self.ydf.iloc[test_indexes]
    
