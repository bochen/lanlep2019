'''
Created on Apr 6, 2018

@author: bo
'''

import os
import utils

tick_per_sec=2.597512307545933e-07
zero_failure_to_peak_sec=0.315497
quake_length={1: 11.540566395569977,
 2: 14.180203599212305,
 3: 8.856243408447243,
 4: 12.693878224447909,
 5: 8.05509606419685,
 6: 7.059247509412199,
 7: 16.107000305270187,
 8: 7.906144318432936,
 9: 9.637176132666621,
 10: 11.4267247041647,
 11: 11.0245547308509,
 12: 8.828581201128031,
 13: 8.565787763958841,
 14: 14.7515396785698,
 15: 9.459497978791095}

quake_weights={1: 7.903253460902388,
 2: 4.1204295533508954,
 3: 7.212928061608938,
 4: 7.176061206274759,
 5: 3.1690071931048647,
 6: 9.83903397840828,
 7: 5.527431626478293,
 8: 3.3810464957744943,
 9: 9.523910652643734,
 10: 6.606302326425366,
 11: 4.4334154594192885,
 12: 9.71276096648772,
 13: 5.709551678391608,
 14: 6.553191829827333,
 15: 9.131675510902053}

quake_weights={
1:1.25,
2:0.666666666666667,
3:0.25,
4:1.25,
5:0.25,
6:0.25,
7:0.666666666666667,
8:0.25,
9:0.25,
10:1.25,
11:1.25,
12:0.25,
13:0.25,
14:0.666666666666667,
15:0.25,
}

target_lengths=[
11.4252837645,
12.6407394841,
11.6683749084,
7.0496431738,
11.4252837645,
16.5301977869,
9.723645757,
12.6407394841,
16.5301977869,
]


if 'LANLEP_HOME' not in os.environ:
    HOME = os.path.join(os.environ['HOME'], 'lanlep2019')
else:
    HOME = os.environ['LANLEP_HOME']

assert HOME is not None 

INPUT_PATH = os.path.join(HOME, "input")
TASK_PATH = os.path.join(INPUT_PATH, 'task')
INFO_PATH = os.path.join(INPUT_PATH, 'info')
SEG_PATH = os.path.join(INPUT_PATH, 'seg')
VEC_PATH = os.path.join(INPUT_PATH, 'vec')
VEC2_PATH = os.path.join(INPUT_PATH, 'vec2')
FEAT_PATH = os.path.join(INPUT_PATH, 'feat')
TAR_PATH = os.path.join(INPUT_PATH, 'tar')
[utils.create_dir_if_not_exists(directory) for directory in [INPUT_PATH, TASK_PATH, INFO_PATH, SEG_PATH, VEC_PATH,VEC2_PATH, FEAT_PATH, TAR_PATH]]

TRAIN_ZIP = os.path.join(INPUT_PATH, 'train.csv.zip')
TRAIN_PARQ = os.path.join(INPUT_PATH, 'train.parq')

TEST_ZIP = os.path.join(INPUT_PATH, 'test.zip')
TEST_PARQ = os.path.join(INPUT_PATH, 'test.parq')


def get_seg_path(name, category1="", category2=""):
    path = SEG_PATH
    if category1:
        path = utils.create_dir_if_not_exists(os.path.join(SEG_PATH, category1))
    if category2: 
        assert category1        
        path = utils.create_dir_if_not_exists(os.path.join(SEG_PATH, category1, category2))
    return os.path.join(path, name)


def get_segs(category1="", category2=""):
    path = SEG_PATH
    if category1:
        path = (os.path.join(SEG_PATH, category1))
    if category2: 
        assert category1        
        path = (os.path.join(SEG_PATH, category1, category2))
    flist = os.listdir(path)
    return {os.path.basename(u).replace(".npz", ""):os.path.join(path, u) for u in flist}

