import fastparquet 
import numpy as np
import config, utils
from task import Task
import os
import pandas as pd
from sklearn.decomposition import PCA


def make_fwv_test_dis(distype):

    def read(name):
        file1 = os.path.join(config.FEAT_PATH, name + "_fwv_offset_0.parq")
        file2 = os.path.join(config.FEAT_PATH, name + "_fwv_offset_125.parq")
        a = fastparquet.ParquetFile(file1).to_pandas()
        b = fastparquet.ParquetFile(file2).to_pandas()
        c = (a + b) / 2.0
        assert c.shape == b.shape
        assert c.shape == a.shape
        return c 
        
    def f(name, testdf, logger):
        
        outputpath = os.path.join(config.FEAT_PATH, name + "_fwv{}_offset_.parq".format(distype))

        testX = testdf.drop("y", axis=1).values
        df = read(name)
        dfX = df.drop("y", axis=1).values
        if distype == "dis":
            r = np.sqrt(np.matmul(dfX, testX.T))
        elif distype == "dis2":
            X = []
            for x in testX:
                a = dfX - x.reshape([1, -1])
                c = np.sqrt(np.sum(a * a, 1))
                X.append(c)
            X = np.array(X).T            
            r = X
        elif distype == 'softmax':
            a = np.sqrt(np.matmul(dfX, testX.T))
            b = np.exp(a)
            r = b / np.sum(b, axis=1, keepdims=True)
        elif distype == 'cos':
            a = np.matmul(dfX, testX.T)
            b = np.sqrt(np.sum(dfX * dfX, axis=1, keepdims=True))
            c = np.sqrt(np.sum(testX * testX, axis=1, keepdims=True))
            print a.shape, b.shape, c.shape
            r = a / np.matmul(b, c.T)

        else:
            raise Exception("NA " + distype)
        
        newdf = pd.DataFrame(r, index=df.index, columns=['fwv{}_{}'.format(distype, i) for i in range(len(testX))])
        newdf['y'] = df['y']
        logger.info("df.head\n" + str(newdf.head()));
        logger.info("Writing " + outputpath)
        fastparquet.write(outputpath, newdf, compression="SNAPPY")
        
    testdf = read("test")
    for name in ['test', 'train_seq_offset_000000', 'train_seq_offset_037500', 'train_seq_offset_075000', 'train_seq_offset_112500', 'dense_train_seq']:
        task = Task('make_fwv_test_dis-{}-{}'.format(name, distype), lambda u: f(name, testdf, u))
        task()


def make_fwv_pca10(distype):

    def read(name):
        file1 = os.path.join(config.FEAT_PATH, name + "_fwv{}_offset_.parq".format(distype))
        a = fastparquet.ParquetFile(file1).to_pandas()
        return a 
        
    def f(logger):
        names = ['test', 'train_seq_offset_000000', 'train_seq_offset_037500', 'train_seq_offset_075000', 'train_seq_offset_112500', 'dense_train_seq']
        lst1 = []
        for name in names:
            lst1.append(read(name))
        df1 = pd.concat(lst1)
        logger.info("df.shape=" + str(df1.shape))
        logger.info("df.head\n" + str(df1.head()));
        X = df1.drop('y', axis=1).values
        logger.info("X.shape=" + str(X.shape))

        n_components = 10
        pca = PCA(n_components=n_components, svd_solver='randomized')
        pca.fit(X)
        logger.info(str(np.sum(pca.explained_variance_ratio_)))
        logger.info(str(pca.explained_variance_ratio_))  
        
        for name in names:
            fname2 = os.path.join(config.FEAT_PATH, name + "_fwv{}_pca10_offset_.parq".format(distype))
            adf = read(name)
            ax = adf.drop('y', axis=1).values
            atx = pca.transform(ax)
            adf2 = pd.DataFrame(atx, index=adf.index, columns=["fwvdis_pca{}_{}".format(n_components, i) for i in range(n_components)])
            adf2['y'] = adf['y']
            logger.info("Writing " + fname2)
            fastparquet.write(fname2, adf2, compression='SNAPPY')
        
    task = Task('make_fwv_pca10-{}'.format(distype), lambda u: f(u))
    task()

           
def run():
    for distype in ['dis', 'dis2', 'softmax', 'cos']:
        make_fwv_test_dis(distype)

    for distype in ['dis', 'dis2', 'softmax', 'cos']:
        make_fwv_pca10(distype)

        
if __name__ == '__main__':
        run()

