import fastparquet 
import pandas as pd 
import numpy as np
import config, utils
from task import Task
import os
from data import QuakeSeg
 

def train2parq():

    def f(logger):
        if utils.file_exists(config.TRAIN_PARQ):
            logger.info("file exists! skip")
            return 
        fpath = config.TRAIN_ZIP
        assert utils.file_exists(fpath), fpath
        df = pd.read_csv(fpath, dtype={"time_to_failure":np.float32, "acoustic_data":np.int32})
        logger.info("\n%s", df.head())
        fastparquet.write(config.TRAIN_PARQ, df , compression='SNAPPY')
        assert utils.file_exists(config.TRAIN_PARQ), config.TRAIN_PARQ

    task = Task('train2parq', lambda u: f(u))
    task()

    
def test2parq():

    def f(logger):
        
        fpath = config.TEST_ZIP
        assert utils.file_exists(fpath), fpath
        
        curr_dir = os.getcwd()
        logger.info("cwd: " + curr_dir)
        tmp_dir = os.path.join(config.INPUT_PATH, "tmp_test")
        utils.remove_if_file_exit(tmp_dir, is_dir=True)
        utils.create_dir_if_not_exists(tmp_dir)
        os.chdir(tmp_dir)
        logger.info("change to: " + os.getcwd())    
        utils.shell_run_and_wait("unzip -q {}".format(fpath))
        
        filelist = [f for f in os.listdir(".") if os.path.isfile(f) and f.startswith("seg") and f.endswith('.csv')]
        logger.info("there are {} test files".format(len(filelist)))
         
        for name in filelist:
            df = pd.read_csv(name)
            df['acoustic_data'] = df['acoustic_data'].astype(np.int32)
            seg_id = name.split(".")[0]
            fname = config.get_seg_path(seg_id + ".npz", "test")
            logger.info("write " + fname)
            qs = QuakeSeg(df=df, name =seg_id)
            qs.save(fname)
        utils.remove_if_file_exit(tmp_dir, is_dir=True)

    task = Task('test2parq', lambda u: f(u))
    task()


def split_train():

    def f(logger):

        fpath = config.TRAIN_PARQ
        assert utils.file_exists(fpath), fpath
        logger.info("read "+fpath)
        df = fastparquet.ParquetFile(fpath).to_pandas()
        logger.info("\n%s", df.head())
        
        a = np.diff(df.time_to_failure.values, 1)
        b = a[a > 2]
        logger.info("quake durations: " + str(b))
        logger.info("mean quake durations: " + str(np.mean(b)))
        logger.info("median quake durations: " + str(np.median(b)))
        logger.info("max quake durations: " + str(np.max(b)))
        logger.info("min quake durations: " + str(np.min(b)))
        
        pos = list(np.where(a > 2)[0])
        logger.info("quakes delimit at " + str(pos))
        for u in pos:
            assert (df.iloc[u]['time_to_failure'] < 0.01) and (df.iloc[u + 1]['time_to_failure'] > 2), "pos " + str(u)  
        
        pos = [0] + pos + [len(df)]
        quakes = []
        for i in range(len(pos) - 1):
            u = pos[i] + 1
            v = pos[i + 1] + 1
            quakes.append(df.iloc[u:v].reset_index(drop=True))
        logger.info("quake duration by 4M hz: " + str(np.round([len(u) * 1.0 / 4000000 for u in quakes], 3)))

        for i, q in enumerate(quakes):
            s = q.acoustic_data
            idx = np.where(s > s.max() * 0.8)[0].max()
            logger.info("quake {}, max signal={}, len={}, max_sig_time={}".format(i, s.max(), (len(s) - idx), q.iloc[idx]['time_to_failure']))

        for i, q in enumerate(quakes):
            outfname = os.path.join(config.INPUT_PATH, "quake_{}.parq".format(i))
            logger.info("write {}".format(outfname))
            logger.info("\n%s", q.head())
            fastparquet.write(outfname, q , compression='SNAPPY')

    task = Task('split_train', lambda u: f(u))
    task()


def quake2mat():

    def f(logger):
        from scipy import io     
        sigs = {}
        for i in range(17):
            fpath = os.path.join(config.INPUT_PATH, 'quake_{}.parq'.format(i))
            assert utils.file_exists(fpath), fpath
            sigs['sig{}'.format(i)] = fastparquet.ParquetFile(fpath).to_pandas()['acoustic_data'].values
            #if i > 2: break
            
        fpath = os.path.join(config.INPUT_PATH, 'train_sig.mat')
        logger.info("write %s", fpath)
        io.savemat(fpath, sigs)
        
    task = Task('quake2mat', lambda u: f(u))
    task()

           
def run():
    train2parq()
    test2parq()
    split_train()
    #quake2mat()


if __name__ == '__main__':
        run()

