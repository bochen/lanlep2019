import pandas as pd 
import numpy as np
import  utils
import sys
import data
import config
import lightgbm as lgb

logger = utils.get_logger("fs_step_backward")

datatuple = None     


def load_data(excludes):
    if 1:
        strategy = {'type':'byquake', "test_quakes":[[ 9, 4, 6] , [15, 1, 5], [10, 14, 3], [7, 11, 2], [8, 13, 12]]}
        types = ['kaggle', 'signal', 'kaggle2'] + ['fwv', 'fwvext'] + ['fwvsoftmax', 'fwvdis', 'fwvdis2', 'fwvcos'] + \
            ['fwvextsoftmax', 'fwvextdis', 'fwvextdis2', 'fwvextcos'] + ['fwvwholereg', 'fwvextwholereg']
        traintest = data.TrainTest('dense', coverages=None, types=types, transform=None, valid_strategy=strategy)

    testdf = traintest.get_test_data()
    datadf = traintest.traindf
    ydf = traintest.ydf
    qdf = pd.DataFrame(datadf.index.map(lambda u: u.split("_")[1]).astype(np.int8), index=datadf.index)

    quakes = range(1, 16)
    idx = qdf['index'].isin(set(quakes)).values
    ydf = ydf[idx]
    qdf = qdf[idx]
    datadf = datadf[idx]
    
    quake_duration = config.quake_length
    for qno in quakes:
        r = 11 / quake_duration[qno]
        print qno, r
        ydf.loc[qdf['index'] == qno] *= r
    
    includes=[True]*450
    for u in excludes:
        includes[u]=False
    datadf = datadf.loc[:, includes]
    
    print datadf.shape, ydf.shape, qdf.shape
    
    return datadf, ydf, qdf


best_mae = 11111
params = {'num_leaves': 54,
          'min_data_in_leaf': 79,
          'objective': 'mae',
          'max_depth':-1,
          'learning_rate': 0.01,
          "boosting": "gbdt",
          "bagging_freq": 5,
          "bagging_fraction": 0.8126672064208567,
          "bagging_seed": 11,
          "metric": 'mae',
          "verbosity":-1,
          'reg_alpha': 0.1302650970728192,
          'reg_lambda': 0.3603427518866501
         }


def train_quake(qno, params, n_thread):
    datadf, ydf, qdf = datatuple
    assert(qno in range(1, 16))
    print 'valid quake', qno
    teidx = (qdf['index'] == qno).values
    tridx = ~qdf['index'].isin({qno, 0, 16}).values
    
    Xtr, ytr, Xte, yte = datadf[tridx], ydf[tridx], datadf[teidx], ydf[teidx],
    Xtr, ytr, Xte, yte = [u.values for u in [Xtr, ytr, Xte, yte]]
    print Xtr.shape, ytr.shape, Xte.shape, yte.shape
    model = lgb.LGBMRegressor(n_estimators=5000, num_threads=n_thread, **params)
    model.fit(Xtr, ytr,
                    eval_set=[(Xtr, ytr), (Xte, yte)], eval_metric='mae',
                    verbose=100, early_stopping_rounds=100)
    return model.best_score['valid_1']['l1'], model.best_iteration


def lgbcv(num_leaves, reg_alpha, reg_lambda, max_depth, subsample, colsample_bytree, min_data_in_leaf, min_sum_hessian_in_leaf):
    d = locals()
    newparams = params.copy()
    newparams.update(d)    
    newparams['num_leaves'] = int(num_leaves)
    newparams['max_depth'] = int(max_depth)
    newparams['min_data_in_leaf'] = int(min_data_in_leaf)
    scores = []
    validScores = {}
    n_thread = 16
    for qno in range(1, 15):
        s = train_quake(qno, newparams, n_thread)
        print "quake ", qno, s
        validScores[qno] = s
        scores.append(s[0])
    r = np.mean(scores)
    global best_mae
    if best_mae > r: best_mae = r
    print "mean score", r
    return -r


def run(excludes):
    excludes2 = [int(i) for i in sys.argv[1].split(",")]

    global datatuple
    datatuple = load_data(excludes2)
    
    a = {'colsample_bytree': 0.32176730266041453,
  'max_depth': 3.2942386945456024,
  'min_data_in_leaf': 1519.3431788315413,
  'min_sum_hessian_in_leaf': 0.4299396260267727,
  'num_leaves': 127.33810239137438,
  'reg_alpha': 2.7531147838832046,
  'reg_lambda': 0.0849284894571881,
  'subsample': 0.622213391736061}
    
    score = -lgbcv(**a)
    print "SCORE: {} {}".format(excludes, score)
    

if __name__ == '__main__':
    if len(sys.argv) <> 2:
        raise Exception("error")
    excludes = sys.argv[1]
    print "Excludes ", str(excludes)
    run(excludes)    

