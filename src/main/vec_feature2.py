import numpy as np
import utils
from rand_proj import set_normalizer
import cPickle as pickle
from data import QuakeSeg
import rand_proj

logger = utils.get_logger("vec_featuer2")


def read_embedding(fname):
    logger.info("reading " + fname)
    with open(fname) as fin:
        lines = list(fin)
    assert len(lines) > 1
    first_line = lines[0]
    num_word, dim_vec = [int(u) for u in first_line.split(" ")]
    assert len(lines) == num_word + 1
    ret = {}
    for line in lines[1:]:
        lst = line.strip().split(" ")
        assert len(lst) == dim_vec + 1, line
        if lst[0] == '</s>': continue
        word = int(lst[0])
        vec = np.array([float(u) for u in lst[1:]])
        ret[word] = vec
    logger.info("dim={}, #word={}".format(dim_vec, len(ret)))
    return ret, dim_vec


class FeatureMaker(object):

    def __init__(self, vecfile, normalizerpath, hasherpath):
        self.embedding, self.dim_vec = read_embedding(vecfile)
        set_normalizer(pickle.load(open(normalizerpath, 'rb')))
        self.rp = (pickle.load(open(hasherpath, 'rb')))
    
    def make_feature(self, word_seq):
        vec = [self.embedding[u] for u in word_seq if u in self.embedding]
        return (np.mean(vec, 0) if len(vec) > 0 else None)
    
    def make_seg_feature(self, segpath, offset,sublength):
        seg = QuakeSeg.load(segpath)
        sig=seg.signal.astype(np.float)
        featseq = rand_proj.create_feature(sig, offset, sublength, parallel=False, extend=True)
        words = self.rp.hash_seq(featseq)
        vec = [self.embedding[u] for u in words if u in self.embedding]
        return (np.mean(vec, 0) if len(vec) > 0 else None), float(seg.failtime)

    
if __name__ == '__main__':
    fm = FeatureMaker("../../input/")
