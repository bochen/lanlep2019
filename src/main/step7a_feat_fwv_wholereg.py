import fastparquet 
import numpy as np
import config, utils
from task import Task
import os
import pandas as pd
import pickle


def make_fwv_wholereg_model():

    def read_train(logger):
        fpath = config.TRAIN_PARQ
        assert utils.file_exists(fpath), fpath
        logger.info("read " + fpath)
        df = fastparquet.ParquetFile(fpath).to_pandas()
        logger.info("\n%s", df.head())
        
        a = np.diff(df.time_to_failure.values, 1)
        b = a[a > 2]
        logger.info("quake durations: " + str(b))
        logger.info("mean quake durations: " + str(np.mean(b)))
        logger.info("median quake durations: " + str(np.median(b)))
        logger.info("max quake durations: " + str(np.max(b)))
        logger.info("min quake durations: " + str(np.min(b)))
        
        pos = list(np.where(a > 2)[0])
        logger.info("quakes delimit at " + str(pos))
        for u in pos:
            assert (df.iloc[u]['time_to_failure'] < 0.01) and (df.iloc[u + 1]['time_to_failure'] > 2), "pos " + str(u)  
        
        pos = [0] + pos + [len(df)]
        quakes = []
        for i in range(len(pos) - 1):
            u = pos[i] + 1
            v = pos[i + 1] + 1
            quakes.append(df.iloc[u:v].reset_index(drop=True))
        logger.info("quake duration by 4M hz: " + str(np.round([len(u) * 1.0 / 4000000 for u in quakes], 3)))
        return df, pos
        
    def f(logger):
        
        outputpath = os.path.join(config.FEAT_PATH, "fwv_wholereg_model.pkl")
        df, pos = read_train(logger)
        
        released = (df['acoustic_data'] ** 2).cumsum()
        delimits = dict(enumerate([u + 1 for u in pos]))
        del delimits[0], delimits[17], delimits[16]
        logger.info(str(delimits))
                
        if 1:
            a = fastparquet.ParquetFile(os.path.join(config.FEAT_PATH, "dense_train_seq_fwv_offset_0.parq")).to_pandas()
            b = fastparquet.ParquetFile(os.path.join(config.FEAT_PATH, "dense_train_seq_fwv_offset_125.parq")).to_pandas()
            traintest = (a + b) / 2
            
        traindf = traintest.drop('y', axis=1)
        ydf = traintest.y
        qdf = traindf.index.map(lambda u: int(u.split("_")[1]))
        idx = ~qdf.isin({0, 16})
        traindf = traindf[idx]
        ydf = ydf[idx]
        qdf = qdf[idx]
        
        def f(u):
            qno, i, offset = [int(x) for x in u.split('_')[1:]]
            return delimits[qno] + offset + (i + 1) * 150 * 1000
        
        globaloffset = pd.Series(traindf.index.map(f), index=traindf.index) 
        
        BT0 = released.loc[globaloffset.values].values
        T0 = globaloffset
        H = np.concatenate([traindf.values, np.ones([len(traindf), 1]), -T0.values.reshape([-1, 1])], axis=1)
        
        x, residuals, rank, s = np.linalg.lstsq(H, -BT0)
        
        from sklearn.linear_model import LinearRegression
        reg = LinearRegression().fit(H, -BT0)
        logger.info("score=" + str(reg.score(H, -BT0)))

        alpha_prime = x[:100]
        beta_prime = x[100]
        k1 = x[101]
        k2 = k1 - 135.6809971137359
        alpha = alpha_prime / k2
        
        def g():
            tmp = ydf.to_frame().reset_index()
            tmp['qno'] = qdf
            tmp = tmp[tmp['y'] >= config.zero_failure_to_peak_sec]
            tmp = tmp.sort_values(['qno', 'y'])
            minsegs = tmp.groupby("qno").head(1).set_index('index')['y']
            return minsegs

        minsegs = g()
        X = np.sum(alpha * traindf.loc[minsegs.index].values, 1)
        beta = -X.mean()
        beta = beta * 1.073
        A = k2 * beta - beta_prime
        
        model_parmas = (alpha, beta, k1, k2, A)
        
        logger.info("Params= " + str(model_parmas))
        
        logger.info("Writing " + outputpath)
        pickle.dump(model_parmas, open(outputpath, 'wb'))

    task = Task('make_fwv_wholereg_model', lambda u: f(u))
    task()


def make_fwv_wholereg_feat():

    def read(name):
        file1 = os.path.join(config.FEAT_PATH, name + "_fwv_offset_0.parq")
        file2 = os.path.join(config.FEAT_PATH, name + "_fwv_offset_125.parq")
        a = fastparquet.ParquetFile(file1).to_pandas()
        b = fastparquet.ParquetFile(file2).to_pandas()
        c = (a + b) / 2.0
        assert c.shape == b.shape
        assert c.shape == a.shape
        return c 
              
    def f(name, logger):
        
        modelpath = os.path.join(config.FEAT_PATH, "fwv_wholereg_model.pkl")
        (alpha, beta, k1, k2, A) = pickle.load(open(modelpath, 'rb'))
        
        df = read(name)
        xdf = df.drop('y', axis=1)
        
        b = np.sum(xdf.values * alpha, 1) + beta
        b = b * config.tick_per_sec
        
        newdf = pd.DataFrame(b, index=df.index, columns=['fwv_wholereg'])
        newdf['y'] = df['y']
        logger.info("df.head\n" + str(newdf.head()));
        outputpath = os.path.join(config.FEAT_PATH, name + "_fwvwholereg.parq")
        logger.info("Writing " + outputpath)
        fastparquet.write(outputpath, newdf, compression="SNAPPY")

    for name in ['test', 'train_seq_offset_000000', 'train_seq_offset_037500', 'train_seq_offset_075000', 'train_seq_offset_112500', 'dense_train_seq']:
        task = Task('make_fwv_wholereg_feat-{}'.format(name), lambda u: f(name, u))
        task()
    
           
def run():
    make_fwv_wholereg_model()
    make_fwv_wholereg_feat()    
            
        
if __name__ == '__main__':
        run()

