import fastparquet 
import numpy as np
import config, utils
from task import Task
import os
import data
import rand_proj
import cPickle as pickle
from rand_proj import RandProj, set_normalizer
from tqdm import tqdm 
import collections
import pandas as pd


def make_s1000(offset=0):
    N = 1000
    n_thread = utils.get_num_thread()
    
    def create_feature(s, offset):
        return rand_proj.create_feature(s, offset, sublength=N, parallel=True)

    def f(logger):
        logger.info("Use {} processes".format(n_thread))

        outputpath = os.path.join(config.VEC_PATH, "{}_k{}_o{}.npz".format('train', N, offset))
        if utils.file_exists(outputpath):
            logger.info("Skip {}".format(outputpath))
        else:
            train_sig = fastparquet.ParquetFile(config.TRAIN_PARQ).to_pandas()['acoustic_data'].values
            logger.info("Processing train sig ...")
            ret = create_feature(train_sig, offset)
            logger.info("Finish creating train sig, shape=" + str(ret.shape))
            logger.info("Writing " + outputpath)
            np.savez_compressed(outputpath, arr=ret)

        test_sigs = data.load_segs("test")
        for i, segname in enumerate(test_sigs):
            outputpath = os.path.join(config.VEC2_PATH, "{}_k{}_o{}.npz".format(segname, N, offset))
            if utils.file_exists(outputpath):
                logger.info("Skip {}".format(outputpath))
                continue
            logger.info("Processing test {} ...".format(i))
            ret = create_feature(test_sigs[segname].signal, offset)
            logger.info("Finish creating test seg {}, shape={}".format(i, str(ret.shape)))
            logger.info("Writing " + outputpath)
            np.savez_compressed(outputpath, arr=ret)

    task = Task('make_s1000-offset_{}'.format(offset), lambda u: f(u))
    task()


def _read_all_vec1000(logger, concat=True):
    files1 = [u for u in os.listdir(config.VEC_PATH) if u.endswith('.npz')]
    files2 = [u for u in os.listdir(config.VEC2_PATH) if u.endswith('.npz')]
    logger.info("has {} files: {}".format(len(files1)+len(files2), str(files1[:5])+ str(files2[:5])))
    assert len(files1) + len(files2) > 1
    
    lst = [ ]
    for fname in files1:
        arr = np.load(os.path.join(config.VEC_PATH, fname))['arr']
        lst.append(arr)
    for fname in files2:
        arr = np.load(os.path.join(config.VEC2_PATH, fname))['arr']
        lst.append(arr)
    if concat:
        lst = np.concatenate(lst).astype(np.float)
        logger.info("array size: " + str(lst.shape))
    else:
        logger.info("List size: " + str(len(lst)))
    return lst 


def make_vec1000_normalizer():

    def f(logger):
        outputpath = os.path.join(config.VEC_PATH, "normalizer.pkl")
        lst = _read_all_vec1000(logger)
        m = np.mean(lst, 0)
        s = np.std(lst, 0)
        
        logger.info("mean: " + str(list(m)))
        logger.info("std: " + str(list(s)))
        logger.info("write " + outputpath)
        pickle.dump((m, s), open(outputpath, 'wb'))
        
    task = Task('make_vec1000_normalizer', lambda u: f(u))
    task()    


def make_vec1000_hasher():

    def f(logger):
        outputpath = os.path.join(config.VEC_PATH, "hasher.pkl")
        lst = _read_all_vec1000(logger)
        set_normalizer(pickle.load(open(os.path.join(config.VEC_PATH, "normalizer.pkl"), 'rb')))
        featsize = len(lst[0])
        rp = RandProj(feat_size=featsize, hash_size=15)
        rp.create_hash(lst)
        pickle.dump(rp, open(outputpath, 'wb'))
        
    task = Task('make_vec1000_hasher', lambda u: f(u))
    task()    


def hash_vec1000():

    def f(logger):
        wc = collections.defaultdict(int)
        outputpath = os.path.join(config.VEC2_PATH, "data.seq")        
        set_normalizer(pickle.load(open(os.path.join(config.VEC_PATH, "normalizer.pkl"), 'rb')))
        rp = (pickle.load(open(os.path.join(config.VEC_PATH, "hasher.pkl"), 'rb')))
        lst = _read_all_vec1000(logger, concat=False)

        with open(outputpath, 'wt') as fout:
            for seq in tqdm(lst):
                hashed = rp.hash_seq(seq)
                for u in hashed: wc[u] += 1
                line = " ".join([str(u) for u in hashed])
                fout.write(line)
                fout.write("\n")
        
        s = pd.Series(wc)
        vc = s.value_counts()
        print vc.head(20)
        print (vc / vc.sum()).head(20)
        logger.info("total: {}, #word<5: {} {}".format(len(s), vc[vc.index < 5].sum(), (vc[vc.index < 5] / vc.sum()).sum()))

    task = Task('hash_vec1000', lambda u: f(u))
    task()    

    
def make_vec1000_model():

    def f(logger):
        cmd = 'fastseq skipgram -dim 100 -lr 0.06 -epoch 200 -thread {} -input data.seq -output model'.format(utils.get_num_thread())
        logger.info("Running " + cmd)
        status = utils.shell_run_and_wait(cmd, config.VEC2_PATH)
        assert status == 0, "status: " + str(status)        

    task = Task('make_vec1000_model', lambda u: f(u))
    task()       

           
def run():
    make_s1000(offset=0)
    make_s1000(offset=125)
    make_vec1000_normalizer()
    make_vec1000_hasher()
    hash_vec1000()
    make_vec1000_model()


if __name__ == '__main__':
        run()

