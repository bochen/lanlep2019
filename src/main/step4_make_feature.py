import fastparquet 
import pandas as pd 
import numpy as np
import config, utils
from task import Task
import os
from data import QuakeSeg
import kaggle_feature
from joblib import Parallel, delayed
from tqdm import tqdm 
import vec_feature
import cPickle as pickle
from multiprocessing import Pool
import signal_feature
import sys
import kaggle2_feature

    
def make_kaggle_feature(name):

    def process_one(segname, segpath):
        seg = QuakeSeg.load(segpath)
        x = kaggle_feature.make_feature(seg.signal)
        y = float(seg.failtime)
        return segname, x, y 
        
    def f(logger):
        d = config.get_segs(name)
        ret = Parallel(n_jobs=utils.get_num_thread())(delayed(process_one)(*v) for v in tqdm(d.items()))
        
        indexes = [u[0] for u in ret]
        X = [u[1] for u in ret]
        Y = [u[2] for u in ret]
        df = pd.DataFrame(X, index=indexes, dtype=np.float32, columns=kaggle_feature.COLUMNS)
        df['y'] = Y
        logger.info("df.shape={}\n{}".format(df.shape, str(df.head())))
        for col in df.columns:
            logger.info("null of {}: {}".format(col, df[col].isnull().mean()))
        
        outname = os.path.join(config.FEAT_PATH, name + ".parq")
        logger.info("write " + outname)
        fastparquet.write(outname, df)
        
    task = Task('make_kaggle_feature-{}'.format(name), lambda u: f(u))
    task()

def make_kaggle2_feature(name):

    def process_one(segname, segpath):
        seg = QuakeSeg.load(segpath)
        x = kaggle2_feature.make_feature(seg.signal)
        y = float(seg.failtime)
        return segname, x, y 
        
    def f(logger):
        d = config.get_segs(name)
        ret = Parallel(n_jobs=utils.get_num_thread())(delayed(process_one)(*v) for v in tqdm(d.items()))
        
        indexes = [u[0] for u in ret]
        X = [u[1] for u in ret]
        Y = [u[2] for u in ret]
        df = pd.DataFrame(X, index=indexes, dtype=np.float32, columns=kaggle2_feature.COLUMNS)
        df['y'] = Y
        logger.info("df.shape={}\n{}".format(df.shape, str(df.head())))
        for col in df.columns:
            logger.info("null of {}: {}".format(col, df[col].isnull().mean()))
        
        outname = os.path.join(config.FEAT_PATH, name + "_kaggle2.parq")
        logger.info("write " + outname)
        fastparquet.write(outname, df)
        
    task = Task('make_kaggle2_feature-{}'.format(name), lambda u: f(u))
    task()

    
def make_signal_feature(name):

    def process_one(segname, segpath):
        seg = QuakeSeg.load(segpath)
        x = signal_feature.make_feature(seg.signal)
        y = float(seg.failtime)
        return segname, x, y 
        
    def f(logger):
        d = config.get_segs(name)
        ret = Parallel(n_jobs=utils.get_num_thread())(delayed(process_one)(*v) for v in tqdm(d.items()))
        
        indexes = [u[0] for u in ret]
        X = [u[1] for u in ret]
        Y = [u[2] for u in ret]
        df = pd.DataFrame(X, index=indexes, dtype=np.float32, columns=signal_feature.COLUMNS)
        df['y'] = Y
        logger.info("df.shape={}\n{}".format(df.shape, str(df.head())))
        for col in df.columns:
            logger.info("null of {}: {}".format(col, df[col].isnull().mean()))
        
        outname = os.path.join(config.FEAT_PATH, name + "_signal.parq")
        logger.info("write " + outname)
        fastparquet.write(outname, df)
        
    task = Task('make_signal_feature-{}'.format(name), lambda u: f(u))
    task()


logger = utils.get_logger("process_one_for_make_vec_feature")


def process_one_for_make_vec_feature(abc):
    segname, segpath, offset = abc
    logger.info("processing " + segpath)
    global g_vec_featmaker
    x, y = g_vec_featmaker.make_seg_feature(segpath, offset=offset, sublength=1000)
    return segname, x, y 


def make_vec_feature(name, modelpath, offset):
        
    def f(logger):
        d = config.get_segs(name)

        normalizerpath = os.path.join(config.VEC_PATH, "normalizer.pkl")
        hasherpath = os.path.join(config.VEC_PATH, "hasher.pkl")
        global g_vec_featmaker
        g_vec_featmaker = vec_feature.FeatureMaker(modelpath, normalizerpath, hasherpath)
        
        p = Pool(utils.get_num_thread())
        params = [ (u[0], u[1], offset) for u in d.items()]
        ret = p.map(process_one_for_make_vec_feature, params)
        p.close()
        p.join()

        indexes = [u[0] for u in ret]
        X = [u[1] for u in ret]
        Y = [u[2] for u in ret]
        logger.info("{} out of {} segs are None".format(np.sum([u is None for u in X]), len(X)))        
        X = [np.zeros([100]) if u is None else u for u in X]
        df = pd.DataFrame(X, index=indexes, dtype=np.float32, columns=['fwv_' + str(u) for u in range(100)])
        df['y'] = Y
        logger.info("df.shape={}\n{}".format(df.shape, str(df.head())))
        for col in df.columns:
            logger.info("null of {}: {}".format(col, df[col].isnull().mean()))
        
        outname = os.path.join(config.FEAT_PATH, name + "_fwv_offset_{}.parq".format(offset))
        logger.info("write " + outname)
        fastparquet.write(outname, df)
        
    task = Task('make_vec_feature-{}_offset_{}'.format(name, offset), lambda u: f(u))
    task()


def make_mean_median_std(name):
        
    def f(logger):
        if name == 'fwv':
            fnames = ['{}{}_offset_0.parq'.format(u, "_" + name if name else "") for u in 
                    ['test', 'train_seq_offset_000000', 'train_seq_offset_037500', 'train_seq_offset_075000', 'train_seq_offset_112500',
                     'train_seq_coverage_18', 'train_seq_coverage_19', 'train_seq_coverage_20', 'train_seq_coverage_21', 'train_seq_coverage_22', ]]
            fnames += ['{}{}_offset_125.parq'.format(u, "_" + name if name else "") for u in 
                    ['test', 'train_seq_offset_000000', 'train_seq_offset_037500', 'train_seq_offset_075000', 'train_seq_offset_112500',
                     'train_seq_coverage_18', 'train_seq_coverage_19', 'train_seq_coverage_20', 'train_seq_coverage_21', 'train_seq_coverage_22', ]]

        else:
            fnames = ['{}{}.parq'.format(u, "_" + name if name else "") for u in 
                    ['test', 'train_seq_offset_000000', 'train_seq_offset_037500', 'train_seq_offset_075000', 'train_seq_offset_112500',
                     'train_seq_coverage_18', 'train_seq_coverage_19', 'train_seq_coverage_20', 'train_seq_coverage_21', 'train_seq_coverage_22', ]]
        fnames = [os.path.join(config.FEAT_PATH, u) for u in fnames]
        for fname in fnames:
            assert utils.file_exists(fname), fname
        
        df = pd.concat([fastparquet.ParquetFile(u).to_pandas() for u in fnames]).drop('y', axis=1)
        assert 'y' not in df.columns
        logger.info("df.shape=%s", str(df.shape))
        logger.info("df.head=\n%s", str(df.head()))
        
        std = df.std(axis=0)
        m = df.mean(axis=0)
        med = df.median(axis=0)
        Q1 = df.quantile(0.25, axis=0)
        Q3 = df.quantile(0.75, axis=0)
        iqr = Q3 - Q1
        
        assert std.isnull().sum() == 0
        assert m.isnull().sum() == 0
        assert med.isnull().sum() == 0
        assert Q1.isnull().sum() == 0
        assert Q3.isnull().sum() == 0
        logger.info("std zero: "+str(std[std==0]))
        logger.info("iqr zero: "+str(iqr[iqr==0]))
        #assert (std == 0).sum() == 0
        #assert (iqr == 0).sum() == 0
        
        outfilename = os.path.join(config.FEAT_PATH, "{}_mean_std.pkl".format(name if name else 'kaggle'))
        logger.info("write " + outfilename)
        pickle.dump(dict(mean=m, std=std, med=med, q1=Q1, q3=Q3, iqr=iqr), open(outfilename, 'wb'))
                
    task = Task('make_mean_median_std-{}'.format(name if name else 'kaggle'), lambda u: f(u))
    task()

           
def run():
    modelpath = os.path.join(config.VEC_PATH, 'model.vec')
    names = ["test", 'dense_train_seq', 'train_seq_offset_000000', 'train_seq_offset_075000', 'train_seq_offset_037500', 'train_seq_offset_112500'] + \
        ['train_seq_coverage_18', 'train_seq_coverage_19', 'train_seq_coverage_20', 'train_seq_coverage_21', 'train_seq_coverage_22'] + \
        []
        #['sp_train_seq_coverage_18', 'sp_train_seq_coverage_19', 'sp_train_seq_coverage_20', 'sp_train_seq_coverage_21', 'sp_train_seq_coverage_22']
         
    for name in names:
        make_kaggle_feature(name)
    for name in names:
        make_kaggle2_feature(name)        
    for name in names:        
        make_signal_feature(name)
    for name in names:        
        for offset in [0, 125]:
            pass
            make_vec_feature(name, modelpath, offset)
    
    for name in ["fwv", 'signal', '']:
        make_mean_median_std(name)


def run_vec(name, offset):
    modelpath = os.path.join(config.VEC_PATH, 'model.vec')
    make_vec_feature(name, modelpath, offset)


if __name__ == '__main__':
    if len(sys.argv) == 4 and sys.argv[1] == 'vec':
        run_vec(name=sys.argv[2], offset=int(sys.argv[3]))
    else:
        run()

