'''
Created on Feb 7, 2019

@author: bo
'''
import unittest

import numpy as np 
from data import QuakeSeg, TrainTest


class Test(unittest.TestCase):

    def testQuakeSeg(self):
        seg = QuakeSeg(signal=np.array(range(10)), failtime=10, name='testQuakeSeg')
        seg.save("/tmp/testQuakeSeg.npz")
        seg2 = QuakeSeg.load("/tmp/testQuakeSeg.npz")
        print seg2.signal 
        print seg2.name 
        print seg2.failtime

    def testTrainTest(self):
        strategy = {'type':'rand', "test_ratio":0.2}
        for types in [['kaggle'], ['kaggle', 'fwv', 'signal']]:
            for transform in [None, 'standard', 'standard_median']:
                data = TrainTest('offset', offset=0, types=types, transform=transform, valid_strategy=strategy)
                assert data.num_dataset() == 1

    def testTrainTest3(self):
        strategy = {'type':'rand', "test_ratio":0.2}
        for types in [['kaggle'], ['kaggle', 'fwv', 'signal']]:
            for transform in [None, 'standard', 'standard_median']:
                data = TrainTest('coverage', coverages=[18,20], types=types, transform=transform, valid_strategy=strategy)
                assert data.num_dataset() == 1

    def testTrainTest2(self):
        strategy = {'type':'rand', "fold":5}
        for types in [['kaggle'], ['kaggle', 'fwv', 'signal']]:
            for transform in [None]:
                data = TrainTest('offset', offset=0, types=types, transform=transform, valid_strategy=strategy)
                assert data.num_dataset() == 5

        
if __name__ == "__main__":
    # import sys;sys.argv = ['', 'Test.testQuakeSeg']
    unittest.main()
